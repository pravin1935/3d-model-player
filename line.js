


var renderer = new THREE.WebGLRenderer();
var scene = new THREE.Scene();

renderer.setSize(window.innerWidth , window.innerHeight);
document.body.appendChild(renderer.domElement) ;


var camera = new THREE.PerspectiveCamera(90 , 700/240 , 0.1 , 1000);
camera.position.set(0 , 0 , 100);
camera.lookAt(new THREE.Vector3(0 , 0 , 0));

var material = new THREE.LineBasicMaterial({ color: 0x0000ff});


var geometry = new THREE.Geometry();
geometry.vertices.push(new THREE.Vector3(10 , 0  , 0 ));
geometry.vertices.push(new THREE.Vector3(-10 , 0  , 0 ));
geometry.vertices.push(new THREE.Vector3(0 , 10 , 0 ));


var line = new THREE.Line( geometry , material);

scene.add(line);

renderer.render(scene , camera);

