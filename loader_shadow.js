var SCREEN_WIDTH = window.innerWidth;
var SCREEN_HEIGHT = window.innerHeight;

var camera, scene;
var canvasRenderer, webglRenderer;

var container, mesh, geometry, plane;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

init();
animate();

function init() {

    container = document.createElement('div');
    document.body.appendChild(container);

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 100000);
    camera.position.x = 2200;
    camera.position.y = 700;
    camera.lookAt({
        x: 0,
        y: 0,
        z: 0
    });

    scene = new THREE.Scene();
    
    var groundMaterial = new THREE.MeshPhongMaterial({
        color: 0x6C6C6C
    });
    plane = new THREE.Mesh(new THREE.PlaneGeometry(500, 500), groundMaterial);
    plane.rotation.x = -Math.PI / 2;
    plane.receiveShadow = true;

    scene.add(plane);

    // LIGHTS
    scene.add(new THREE.AmbientLight(0x666666));

    var light;

    light = new THREE.DirectionalLight(0xdfebff, 1.75);
    light.position.set(100, 200, 400);
    light.position.multiplyScalar(1.2);

    light.castShadow = true;
    light.shadowCameraVisible = true;

    light.shadowMapWidth = 712;
    light.shadowMapHeight = 712;

    var d = 200;

    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;

    light.shadowCameraFar = 1000;
    light.shadowDarkness = 0.1;

    scene.add(light);
    
    var mtlLoader = new THREE.MTLLoader();
				mtlLoader.setPath( 'https://threejs.org/examples/obj/male02/' );
				mtlLoader.load( 'male02_dds.mtl', function( materials ) {
					materials.preload();
					var objLoader = new THREE.OBJLoader();
					objLoader.setMaterials( materials );
					objLoader.setPath( 'https://threejs.org/examples/obj/male02/' );
					objLoader.load( 'male02.obj', function ( object ) {
          
          var positionX = 0;
        	var positionY = 4;
        	var positionZ = 0;

          object.position.x = positionX;
          object.position.y = positionY;
          object.position.z = positionZ;
          object.scale.x = 1
          object.scale.y = 1;
          object.scale.z = 1;
          
          // castshow setting for object loaded by THREE.OBJLoader()
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.castShadow = true;
            }
          });
        
        	scene.add(object);
				});
			});
    
    

    // RENDERER
    webglRenderer = new THREE.WebGLRenderer();
    webglRenderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    webglRenderer.domElement.style.position = "relative";
    webglRenderer.shadowMapEnabled = true;
    webglRenderer.shadowMapSoft = true;

    container.appendChild(webglRenderer.domElement);
    window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    webglRenderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    var timer = Date.now() * 0.0002;
    camera.position.x = Math.cos(timer) * 1000;
    camera.position.z = Math.sin(timer) * 1000;
    requestAnimationFrame(animate);
    render();
}

function render() {
    camera.lookAt(scene.position);
    webglRenderer.render(scene, camera);
}